import { useContext, useState } from 'react';
import { Fragment } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext';

export default function AppNavbar(){

	/*
		localStorage.getItem is used  to get a piece of information from the browser's local storage. devs usually insert information in the local storage for specific purposes; one of commonly used purpose is for login in. it accepts 1 argument pertaining to the specific key/property inside the local storage to get its value
	*/
	// uses the state hook to set the initial value for user variable coming from the local storage, specifically the email property

	const {user} = useContext(UserContext);
	console.log(user);

	return(
			<Navbar bg="light" expand="lg">
				<Navbar.Brand as={Link} to='/'>ZUITT</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link as={NavLink} to='/' exact>Home</Nav.Link>
						<Nav.Link as={NavLink} to='/courses' exact> Courses</Nav.Link>
						{(user.id !== null) ?
							<Nav.Link as={NavLink} to='/logout' exact> Logout</Nav.Link>
							:
							<Fragment>							
								<Nav.Link as={NavLink} to='/login' exact> Login</Nav.Link>
								<Nav.Link as={NavLink} to='/register' exact> Register</Nav.Link>
							</Fragment>
						}
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}